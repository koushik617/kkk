! function(e) {
    var t = {};

    function n(r) { if (t[r]) return t[r].exports; var o = t[r] = { i: r, l: !1, exports: {} }; return e[r].call(o.exports, o, o.exports, n), o.l = !0, o.exports }
    n.m = e, n.c = t, n.d = function(e, t, r) { n.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: r }) }, n.r = function(e) { "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 }) }, n.t = function(e, t) {
        if (1 & t && (e = n(e)), 8 & t) return e;
        if (4 & t && "object" == typeof e && e && e.__esModule) return e;
        var r = Object.create(null);
        if (n.r(r), Object.defineProperty(r, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e)
            for (var o in e) n.d(r, o, function(t) { return e[t] }.bind(null, o));
        return r
    }, n.n = function(e) { var t = e && e.__esModule ? function() { return e.default } : function() { return e }; return n.d(t, "a", t), t }, n.o = function(e, t) { return Object.prototype.hasOwnProperty.call(e, t) }, n.p = "", n(n.s = 0)
}([function(e, t) {
    function n(e) {
        fetch(e).then(e => e.text()).then(e => {
            let t = null;
            document.getElementById("iframe").contentDocument ? t = document.getElementById("iframe").contentDocument : document.getElementById("iframe").contentWindow.document && (t = document.getElementById("iframe").contentWindow.document), t && (t.open(), t.write(e), t.close())
        }).catch(() => { document.getElementById("engagely-frame").remove() })
    }
    setTimeout((function() {
        ! function() {
            let e = document.createElement("div");
            e.setAttribute("id", "engagely-frame");
            const t = { position: "fixed", bottom: "0", right: "0", "z-index": "1000", margin: "0 !important", padding: "0 !important", "-webkit-overflow-scrolling": "touch !important", "overflow-y": "scroll !important", "height": "-webkit-fill-available" };
            Object.keys(t).forEach((function(n) { e.style[n] = t[n] })), document.body.appendChild(e);
            let r = document.createElement("iframe");
            r.setAttribute("id", "iframe"), r.setAttribute("frameborder", 0), r.setAttribute("allowtransparency", "allowtransparency"), r.setAttribute("allowfullscreen", "allowfullscreen"), r.setAttribute("mozallowfullscreen", "mozallowfullscreen"), r.setAttribute("msallowfullscreen", "msallowfullscreen"), r.setAttribute("oallowfullscreen", "oallowfullscreen"), r.setAttribute("webkitallowfullscreen", "webkitallowfullscreen");
            const o = { height: "100%", width: "100%" };
            Object.keys(o).forEach((function(e) { r.style[e] = o[e] })), e.appendChild(r), n("https://botbuilder.engagely.ai:8080/engagelyscripts/index.html")
        }()
    }), 2e3)
}]);
